import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginTest {
    WebDriver driver;

    @BeforeClass
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void logout(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
    }

    @Test
    public void TC001(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //ambil value username & password
        String username =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
    }

    @Test
    public void TC002(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("salah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test
    public void TC003(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test
    public void TC004(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("salah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }
}
