import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class BokkAppointmentSuccess {
    WebDriver driver;

    @BeforeClass
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void TC001(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //ambil value username & password
        String username =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
        driver.findElement(By.xpath("//*[@id=\"combo_facility\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"chk_hospotal_readmission\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/form/div[3]/div/label[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"txt_visit_date\"]")).sendKeys("12/12/2022");
        driver.findElement(By.xpath("//*[@id=\"txt_comment\"]")).sendKeys("Book Appointment");
        driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();
    }

    @Test
    public void TC002(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //ambil value username & password
        String username =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password =  driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        //Login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
        driver.findElement(By.xpath("//*[@id=\"combo_facility\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"chk_hospotal_readmission\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/form/div[3]/div/label[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"txt_visit_date\"]")).sendKeys("12/12/2022");
        driver.findElement(By.xpath("//*[@id=\"txt_comment\"]")).sendKeys("Book Appointment");
        driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"menu-toggle\"]/i")).click();
        driver.findElement(By.xpath("//*[@id=\"sidebar-wrapper\"]/ul/li[3]/a")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"facility\"]")).getText(), "Tokyo CURA Healthcare Center");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"program\"]")).getText(), "Medicaid");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"comment\"]")).getText(), "Book Appointment");
    }

    @AfterMethod
    public void logout(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }
}
